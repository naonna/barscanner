package com.fivesysdev.barscanner;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.fivesysdev.barscanner.api.BarcodeLookupApi;
import com.fivesysdev.barscanner.api.BestByuApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    public static App sApp;
    private static BarcodeLookupApi barcodeLookupApi;
    private static BestByuApi bestByuApi;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        sApp = this;

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        final Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        barcodeLookupApi = retrofitBuilder.baseUrl("https://api.barcodelookup.com/v2/").build().create(BarcodeLookupApi.class);
        bestByuApi = retrofitBuilder.baseUrl("https://api.bestbuy.com/v1/").build().create(BestByuApi.class);
    }

    public static BarcodeLookupApi getBarcodeLookupApi() {
        return barcodeLookupApi;
    }

    public static BestByuApi getBestByuApi() {
        return bestByuApi;
    }
}
