package com.fivesysdev.barscanner.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.fivesysdev.barscanner.MainActivity
import com.fivesysdev.barscanner.R
import com.fivesysdev.barscanner.models.Product
import com.fivesysdev.barscanner.utils.BarcodeListener
import com.fivesysdev.barscanner.utils.CommonUtils
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_one_button.*
import kotlinx.android.synthetic.main.fragment_add_price.view.*
import java.io.File
import kotlin.math.round


class PriceFragment: Fragment() {

    companion object {
        const val TAG = "PriceFragment"

        fun newInstance(product: Product): InfoFragment{
            val args = Bundle()
            args.putParcelable("product", product)
            val fragment = InfoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    var photoFile: File? = null
    var product: Product? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_add_price, container, false)
        product = arguments?.let {
            it.getParcelable("product") as Product
        }?: Product()

        view.ib_add_photo.setOnClickListener{
            if (activity != null) {
                startPhoneCamera()
            }
        }

        view.btn_next.setOnClickListener{
            proceedProduct()
        }
        return view
    }

    private fun startPhoneCamera() {
        photoFile = CommonUtils.getOutputMediaFile()
        if (photoFile != null){
            (activity as MainActivity).startPhoneCamera(photoFile!!, this)
            product?.priceTagFile = photoFile.toString()
        }
    }

    private fun proceedProduct(){
        if (photoFile == null && context != null){
            Toast.makeText(context, "Please, add photo of price tag!", Toast.LENGTH_SHORT).show()
            return
        }
        view?.let {
            if (it.edit_text.text.isEmpty()){
                Toast.makeText(context, "Please, enter the product's price!", Toast.LENGTH_SHORT).show()
                return
            }
        }

        if (context == null) return
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_one_button)

        val text = dialog.text_dialog
        val price = view?.edit_text?.text.toString().toFloat()
        text.setText("This product will cost you ${round(price/3)} $/month for 3 month")


        val dialogButton = dialog.btn_dialog_close
        dialogButton.setOnClickListener { _ ->
            dialog.dismiss()
            fragmentManager?.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        dialog.show()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (context != null && photoFile != null){

                Single.fromCallable{
                    val myBitmap = BitmapFactory.decodeFile(photoFile!!.absolutePath)
                    val thumbnail = ThumbnailUtils.extractThumbnail(myBitmap,view?.ib_add_photo?.width ?: 100, view?.ib_add_photo?.height ?: 100 )
                    thumbnail }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({bitmap -> view?.ib_add_photo?.setImageBitmap(bitmap)},
                                {error -> Log.d(TAG, error.localizedMessage)})

            }
        } else {
            photoFile?.delete()
        }
    }

}