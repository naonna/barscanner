package com.fivesysdev.barscanner.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import com.fivesysdev.barscanner.CameraActivityNew
import com.fivesysdev.barscanner.MainActivity
import com.fivesysdev.barscanner.R
import com.fivesysdev.barscanner.api.BarcodeLookup
import com.fivesysdev.barscanner.models.Product
import com.fivesysdev.barscanner.utils.BarcodeListener
import com.fivesysdev.barscanner.utils.CommonUtils
import com.fivesysdev.barscanner.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_main.view.*

import java.io.File

class MainFragment : Fragment(), BarcodeListener {

    companion object {
        const val TAG = "MainFragment"
        private const val PERMISSIONS_REQUEST_CAMERA = 1
        private const val REQUEST_CUSTOM_CAMERA = 3
    }

    private var photoFile: File? = null

    private var clickListener = View.OnClickListener { v ->
        when (v.getId()) {
            R.id.btn_custom_camera -> startCustomCamera()
            R.id.btn_phone_camera -> startPhoneCamera()
            R.id.btn_search_product -> {
                if (activity != null){
                    val fragment = SearchFragment.newInstance(false)
                    (activity as MainActivity).launchFragment(fragment, true)
                }
            }
        }
    }

    lateinit var progressBar: ProgressBar


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        view.btn_custom_camera.setOnClickListener(clickListener)
        view.btn_phone_camera.setOnClickListener(clickListener)
        view.btn_search_product.setOnClickListener(clickListener)
        progressBar = view.findViewById(R.id.progressBar)

        return view
    }

    private fun startCustomCamera() {
        if (context == null) return
        val intent = Intent(context, CameraActivityNew::class.java)
        startActivityForResult(intent, REQUEST_CUSTOM_CAMERA)

    }

    private fun startPhoneCamera() {
        photoFile = CommonUtils.getOutputMediaFile()
        if (photoFile != null){
            (activity as MainActivity).startPhoneCamera(photoFile!!, this)
        }
    }


    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        if (photoFile != null)
            savedInstanceState.putString("file", photoFile!!.toString())
        //declare values before saving the state
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults.size > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED
                    && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "Need asked permissions to use this app", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            var file = ""
            if (requestCode == MainActivity.REQUEST_IMAGE_CAPTURE) {
                if (photoFile != null) {
                    file = photoFile!!.toString()
                }
            } else if (requestCode == REQUEST_CUSTOM_CAMERA) {
                file = data!!.getStringExtra("file")
            }
            val firebaseUtils = FirebaseUtils()
            if (context != null)
                firebaseUtils.initialize(context!!)
            if (!file.isEmpty()) {
                showProgressBar(true)
                firebaseUtils.recognizeBarcode(context!!, file, this)
            } else
                showDialog("Can not create the file", "", "")

        }
    }

    override fun sendResult(result: String, expl: String, rawValue: String) {
        val fragment: Fragment
        if (rawValue.isEmpty()) {
            showProgressBar(false)
            fragment = SearchFragment()
            if (activity == null) return
            (activity as MainActivity).launchFragment(fragment, true)
        } else {
            getProductByBarcode(rawValue)
        }

        //        if (!rawValue.isEmpty()){
        //            SemanticsUtils semanticsUtils = new SemanticsUtils();
        //            semanticsUtils.getProductInfo(rawValue);
        //        }
    }



    fun getProductByBarcode(upc: String){
        val upcInt = upc.toLongOrNull()
        Log.d(TAG, "upc = $upc, upcInt = $upcInt")


        val barcodeLookup = BarcodeLookup.getInstance()
        barcodeLookup.getProducts(upcInt).subscribe({
            result ->
            Log.d(InfoFragment.TAG, result.toString())
            val product = result.products?.get(0)
            onSearchResult(product?.getProduct())
        }, { error ->
            Log.d(InfoFragment.TAG, error.localizedMessage)
            onSearchResult()
        })
    }

    fun showProgressBar(show: Boolean){
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun onSearchResult(barcodeProduct: Product? = null){
        val fragment: Fragment
        showProgressBar(false)
        if (barcodeProduct == null){
            showProgressBar(false)
            fragment = SearchFragment()
        } else {
            fragment = InfoFragment.newInstance(barcodeProduct)
        }
        if (activity != null)
            (activity as MainActivity).launchFragment(fragment, true)
    }

    fun showDialog(result: String, expl: String, rawValue: String) {
        if (activity == null) return
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog)
        val text = dialog.findViewById<View>(R.id.text_dialog) as TextView
        text.text = result
        val textMsg = dialog.findViewById<View>(R.id.text_msg) as TextView
        textMsg.text = expl

        val buttonClose = dialog.findViewById<View>(R.id.btn_dialog_close) as Button
        buttonClose.setOnClickListener { v ->
            dialog.dismiss()
            dialogClosed()
        }

        val buttonInfo = dialog.findViewById<View>(R.id.btn_dialog_info) as Button
        buttonInfo.setOnClickListener { v ->
            dialog.dismiss()
            findInfo(rawValue)
        }

        dialog.show()
    }

    override fun dialogClosed() {}

    override fun findInfo(rawValue: String) {
        if (activity == null) return
        val fragment = InfoFragment.newInstance(rawValue)
        (activity as MainActivity).launchFragment(fragment, true)
    }
}
