package com.fivesysdev.barscanner.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.fivesysdev.barscanner.MainActivity
import com.fivesysdev.barscanner.R
import com.fivesysdev.barscanner.models.Product
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.URL
import android.app.Activity
import android.content.Intent
import android.support.v4.content.FileProvider
import com.fivesysdev.barscanner.utils.CommonUtils
import com.fivesysdev.barscanner.utils.FirebaseUtils
import com.fivesysdev.barscanner.utils.PriceListener
import java.io.File


class InfoFragment : Fragment(), PriceListener {

    companion object {
        fun newInstance(upc: String): InfoFragment {
            val args = Bundle()
            args.putString("upc", upc)
            val fragment = InfoFragment()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(product: Product): InfoFragment{
            val args = Bundle()
            args.putParcelable("product", product)
            val fragment = InfoFragment()
            fragment.arguments = args
            return fragment
        }

        const val TAG = "InfoFragment"
    }

    lateinit var tvBarcode: TextView
    lateinit var tvName: TextView
    lateinit var tvDetails: TextView
    lateinit var ivProduct: ImageView
    lateinit var tvPrice: TextView
    lateinit var btnAddPrice: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_info_pro, container, false)

        (activity as MainActivity).showBackButton(true)

        val product = arguments?.getParcelable("product") as Product?

        tvBarcode = view.findViewById(R.id.tv_barcode)
        tvBarcode.text = product?.upc

        tvName = view.findViewById(R.id.tv_product_name)
        tvName.text = product?.productName

        tvDetails = view.findViewById(R.id.tv_product_details)
        tvDetails.text = product?.productDescription

        tvPrice = view.findViewById(R.id.tv_price)
        if (product?.price != null){
            tvPrice.text = "Price: ${product?.price} \n For debug purpose only"
        }

        btnAddPrice = view.findViewById(R.id.btn_add_price)
        btnAddPrice.setOnClickListener{
//            scanPrice()
            val fragment = PriceFragment()
            if (activity != null)
                (activity as MainActivity).launchFragment(fragment, true)
        }

        ivProduct = view.findViewById(R.id.ib_add_photo)
        Single.fromCallable{
            val url = URL(product?.imageUrl)
            val myBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            myBitmap }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{bitmap -> ivProduct.setImageBitmap(bitmap)}
        return view
    }

    private var photoFile: File? = null


    @Deprecated("use 'addPrice' instead of this")
    private fun scanPrice(){
        photoFile = CommonUtils.getOutputMediaFile()
        if (photoFile != null){
            (activity as MainActivity).startPhoneCamera(photoFile!!, this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val firebaseUtils = FirebaseUtils()
            if (context != null && photoFile != null){
                val uri = FileProvider.getUriForFile(
                        context!!, context!!.applicationContext.packageName + ".provider", photoFile!!)
                firebaseUtils.recognizePrice(context!!, uri, this)
            }
        }
    }

    override fun onPriceRecognizeError() {

    }

    override fun onPriceRecognize(price: String) {

    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as MainActivity).showBackButton(false)
    }

}
