package com.fivesysdev.barscanner.fragments

import com.fivesysdev.barscanner.models.Product

interface ProductConverter{
    fun getProduct(): Product
}