package com.fivesysdev.barscanner.fragments

import android.database.MatrixCursor
import android.os.Bundle
import android.provider.BaseColumns
import android.support.v4.app.Fragment
import android.support.v4.widget.CursorAdapter
import android.support.v4.widget.SimpleCursorAdapter
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.fivesysdev.barscanner.MainActivity
import com.fivesysdev.barscanner.R
import com.fivesysdev.barscanner.api.BestByu
import com.fivesysdev.barscanner.models.Product
import kotlinx.android.synthetic.main.fragment_search_product.view.*

class SearchFragment : Fragment() {

    companion object {
        private const val TAG = "SearchFragment"

        fun newInstance(barcodeScanned: Boolean = true): SearchFragment{
            val args = Bundle()
            args.putBoolean("barcodeScanned", barcodeScanned)
            val fragment = SearchFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var searchView: SearchView
    private lateinit var btnSearch: Button
    private lateinit var searchSuggestionsAdapter:SimpleCursorAdapter

    private var listOfProducts = mutableListOf<Product>()

    private var lastSearchQuery: String = ""
    private var choseFromSuggestions = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_search_product, container, false)

        (activity as MainActivity).showBackButton(true)

        initSearchView(view)

        val barcodeScanned = arguments?.getBoolean("barcodeScanned", false) ?: false

        view.tv_title.text = if (barcodeScanned) "We are sorry but we can't find your product"
            else "Please, enter the product's title"

        view.tv_subtitle.text = if (barcodeScanned) "Could  you please enter the product name" else ""


        btnSearch = view.findViewById(R.id.btn_search)

        btnSearch.setOnClickListener {
            val text = searchView.query.toString()
            Log.d(TAG, "text = $text, choseFromSuggestions = $choseFromSuggestions")
            if (!choseFromSuggestions) {
                searchProduct(text)
            } else {
                val product: List<Product>? = listOfProducts.filter { product -> product.productName.equals(text) }
                Log.d(TAG, "chose product = $product")
                if (product != null && !product.isEmpty())
                    onSearchResult(product[0])
                else
                    searchProduct(text)
            }

        }

        return view
    }

    fun initSearchView(view: View){
        searchView = view.findViewById(R.id.edit_text)
        searchView.queryHint = "Enter product name"
        searchSuggestionsAdapter = SimpleCursorAdapter(context,
                android.R.layout.simple_list_item_1,
                null,
                arrayOf("queries"),
                intArrayOf(android.R.id.text1),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        searchView.suggestionsAdapter = searchSuggestionsAdapter


        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(s: String?): Boolean {
                Log.d(TAG, "onQueryTextSubmit")
                lastSearchQuery = searchView.query.toString()
                searchSuggestionsAdapter.changeCursor(null)
                searchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(s: String?): Boolean {
                Log.d(TAG, "onQueryTextChange")
                if (s!=lastSearchQuery)
                    fetchSearchSuggestions(s)
                choseFromSuggestions = false
                return false
            }

        })



        searchView.setOnSuggestionListener(object : SearchView.OnSuggestionListener{
            override fun onSuggestionSelect(s: Int): Boolean {
                Log.d(TAG, "onSuggestionSelect")
                return true
            }

            override fun onSuggestionClick(s: Int): Boolean {
                Log.d(TAG, "onSuggestionClick")
                lastSearchQuery = searchSuggestionsAdapter.cursor.getString(1)
                Log.d(TAG, "lastSearchQuery = $lastSearchQuery")
                searchView.setQuery(lastSearchQuery, false)
                choseFromSuggestions = true
                return true
            }

        })

        searchView.setOnCloseListener {
            searchSuggestionsAdapter.changeCursor(null)
            true
        }
    }

    fun fetchSearchSuggestions(s: String?){
        if (s == null || s.isEmpty()) return
        val bestByu = BestByu.getInstance()
        bestByu.autocomplete(s).subscribe ({ result ->
            listOfProducts = result.products?.map { it -> it.getProduct() }?.toMutableList() ?: mutableListOf()

            val c = MatrixCursor(arrayOf(BaseColumns._ID, "queries"))
            for (i in listOfProducts.indices) {
                c.addRow(arrayOf(i, listOfProducts.get(i).productName))
            }
            searchSuggestionsAdapter.changeCursor(c)
        }, { error ->
            Log.d(TAG, error.localizedMessage)
            onSearchResult()
        })

    }

    fun searchProduct(text: String){
        Log.d(TAG, "--->searchProduct: text = $text")
        if(text.isEmpty()) {
            Toast.makeText(context, "Product name shouldn't be empty", Toast.LENGTH_SHORT).show()
            return
        }
        val bestByu = BestByu.getInstance()
        bestByu.getProducts(text).subscribe({
            result ->
            Log.d(InfoFragment.TAG, result.toString())
            Log.d(TAG, "result: products.size = ${result.products?.size ?: "null"}, products.get(0) =  ${result.products?.get(0) ?: "null"}")
            val product = result.products?.get(0)
            onSearchResult(product?.getProduct())
        }, { error ->
            Log.d(TAG, error.localizedMessage)
            onSearchResult()
        })
        Log.d(TAG, "searchProduct<----")
    }


    fun onSearchResult(product: Product? = null){
        Log.d(TAG, "---->onSearchResult")
        Log.d(TAG, "product = ${product.toString()}")
        val fragment: Fragment
        if (product == null){
            if(context != null)
                Toast.makeText(context, "Didn't find anything", Toast.LENGTH_SHORT).show()
        } else {
            fragment = InfoFragment.newInstance(product)
            if (activity != null)
                fragmentManager?.popBackStack()
                (activity as MainActivity).launchFragment(fragment, true)
        }

        Log.d(TAG, "onSearchResult<----")
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as MainActivity).showBackButton(false)
    }
}
