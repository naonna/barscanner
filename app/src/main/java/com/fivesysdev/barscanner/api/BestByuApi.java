package com.fivesysdev.barscanner.api;

import com.fivesysdev.barscanner.models.BarcodeProductResponse;
import com.fivesysdev.barscanner.models.BestByuProductResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BestByuApi {

    @GET("products(search={search})")
    Observable<BestByuProductResponse> getProducts(
            @Path(value = "search", encoded = true) String search,
            @Query("apiKey") String api_key,
            @Query("pageSize") int pageSize,
            @Query("format") String format);
}
