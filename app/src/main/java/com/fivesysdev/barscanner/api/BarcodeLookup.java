package com.fivesysdev.barscanner.api;

import com.fivesysdev.barscanner.App;
import com.fivesysdev.barscanner.models.BarcodeProductResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BarcodeLookup {

    private static final String API_KEY = "s2p72jfcnwkgw4pi4vje6x99vthsod";

    private static BarcodeLookup instance;

    public static BarcodeLookup getInstance() {
        if (instance == null)
            instance = new BarcodeLookup();
        return instance;
    }

    public Observable<BarcodeProductResponse> getProducts(Long barcode){
        return App.getBarcodeLookupApi().getProducts(barcode, API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
