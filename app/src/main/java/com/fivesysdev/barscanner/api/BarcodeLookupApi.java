package com.fivesysdev.barscanner.api;

import com.fivesysdev.barscanner.models.BarcodeProductResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BarcodeLookupApi {
    @GET("products")
    Observable<BarcodeProductResponse> getProducts(
            @Query("barcode") Long barcode,
            @Query("key") String api_key);
}
