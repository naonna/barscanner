package com.fivesysdev.barscanner.api;

import android.util.Log;

import com.fivesysdev.barscanner.App;
import com.fivesysdev.barscanner.models.BestByuProductResponse;

import java.net.URLEncoder;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BestByu {

    private static final String API_KEY = "w0zqIYuEHTfcm8RPu3kyej3h";

    private static BestByu instance;

    public static BestByu getInstance() {
        if (instance == null)
            instance = new BestByu();
        return instance;
    }

    public Observable<BestByuProductResponse> getProducts(String s){
        s = s.replaceAll(" ", "%20");
        Log.d("BestByu", "getProducts: " + s);
        return App.getBestByuApi().getProducts(s, API_KEY, 1, "json")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<BestByuProductResponse> autocomplete(String s){
        s = s.replaceAll(" ", "%20");
        return App.getBestByuApi().getProducts(s, API_KEY, 15, "json")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
