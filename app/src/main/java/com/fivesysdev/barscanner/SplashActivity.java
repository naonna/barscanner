package com.fivesysdev.barscanner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.fivesysdev.barscanner.utils.BarcodeDialog;
import com.fivesysdev.barscanner.utils.BarcodeListener;
import com.fivesysdev.barscanner.utils.CommonUtils;
import com.fivesysdev.barscanner.utils.FirebaseUtils;
import com.fivesysdev.barscanner.utils.SemanticsUtils;

import java.io.File;
@Deprecated
public class SplashActivity extends AppCompatActivity
implements BarcodeListener
{

    final int PERMISSIONS_REQUEST_CAMERA = 1;
    final int REQUEST_IMAGE_CAPTURE = 2;

    File photoFile;

    View.OnClickListener clickListener = v -> {
        switch (v.getId()){
            case R.id.btn_custom_camera :
                startCustomCamera();
                break;
            case R.id.btn_phone_camera :
                startPhoneCamera();
                break;
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
        setContentView(R.layout.fragment_main);

        if (Build.VERSION.SDK_INT >= 23){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_CAMERA);
            }
        }

        findViewById(R.id.btn_custom_camera).setOnClickListener(clickListener);
        findViewById(R.id.btn_phone_camera).setOnClickListener(clickListener);

        if (savedInstanceState != null){
            photoFile = new File(savedInstanceState.getString("file"));
        }
//        startActivity();
    }

    private void startCustomCamera(){
       startActivity();
    }

    private void startPhoneCamera(){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            photoFile = CommonUtils.getOutputMediaFile();
            if (photoFile != null) {

                Uri uri = FileProvider.getUriForFile(
                        this,
                        getApplicationContext()
                                .getPackageName() + ".provider", photoFile);

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED
                    && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
               Toast.makeText(this, "Need asked permissions to use this app", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            FirebaseUtils firebaseUtils = new FirebaseUtils();
            firebaseUtils.initialize(this);
            if (photoFile != null){
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                firebaseUtils.recognizeBarcode(this, photoFile.toString(), this);
            } else
                BarcodeDialog.showDialog(this, "Can not create the file", "");

        }
    }

    private void startActivity(){
//        Intent intent = new Intent(this, CameraActivity.class);
        Intent intent = new Intent(this, CameraActivityNew.class);
        startActivity(intent);
//        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if (photoFile!=null)
            savedInstanceState.putString("file", photoFile.toString());
        //declare values before saving the state
        super.onSaveInstanceState(savedInstanceState);
    }

    String raw = "";
    @Override
    public void sendResult(String result, String expl, String rawValue) {
        BarcodeDialog.showDialog(this, result, expl);
        findViewById(R.id.progressBar).setVisibility(View.GONE);
        if (!rawValue.isEmpty()){
            SemanticsUtils semanticsUtils = new SemanticsUtils();
            semanticsUtils.getProductInfo(rawValue);
        }
//        raw = rawValue;
    }

    @Override
    public void dialogClosed() {
    }

    @Override
    public void findInfo(String rawValue) {

    }
}
