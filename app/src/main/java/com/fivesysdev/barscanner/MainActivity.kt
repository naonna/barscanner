package com.fivesysdev.barscanner

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import com.fivesysdev.barscanner.fragments.MainFragment
import java.io.File

class MainActivity : AppCompatActivity() {

    companion object {
        const val PERMISSIONS_REQUEST_CAMERA = 1
        const val REQUEST_IMAGE_CAPTURE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        PERMISSIONS_REQUEST_CAMERA)
            }
        }

        if (savedInstanceState == null){
            val fragment = MainFragment()
            launchFragment(fragment, false)
        }

    }

    fun showBackButton(show: Boolean){
        supportActionBar?.setDisplayHomeAsUpEnabled(show)
    }

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStack()
        return true
    }

    fun launchFragment(fragment: Fragment, addToBackStack: Boolean) {
        val tag = fragment.javaClass.simpleName
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, tag)
        if (addToBackStack) transaction.addToBackStack(tag)
        transaction.commitAllowingStateLoss()
    }

    fun startPhoneCamera(photoFile: File, fragment: Fragment) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(packageManager) != null) {

            val uri = FileProvider.getUriForFile(
                    this, applicationContext.packageName + ".provider", photoFile)

            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            fragment.startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)
        }
    }
}
