package com.fivesysdev.barscanner.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
        var upc: String? = null,
        var productName: String? = null,
        var productDescription: String? = null,
        var imageUrl: String? = null,
        var price: Double? = null,
        var priceTagFile: String? = null
) : Parcelable {
    override fun toString(): String {
        return "Product: upc = $upc, productName = $productName, productDescription = $productDescription, imageUrl = $imageUrl, price = $price"
    }
}
