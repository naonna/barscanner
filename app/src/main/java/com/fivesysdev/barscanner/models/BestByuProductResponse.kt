package com.fivesysdev.barscanner.models

import android.os.Parcelable
import com.fivesysdev.barscanner.fragments.ProductConverter
import kotlinx.android.parcel.Parcelize

class BestByuProductResponse {
    var products: Array<Product>? = null

    @Parcelize
    data class Product(
            var name: String? = null,
            var upc: String? = null,
            var thumbnailImage: String? = null,
            var mediumImage: String? = null,
            var description: String? = null,
            var longDescription: String? = null,
            var shortDescription: String? = null,
            var regularPrice: Double? = null
    ) : Parcelable, ProductConverter {

        override fun getProduct(): com.fivesysdev.barscanner.models.Product {
            return com.fivesysdev.barscanner.models.Product(upc, name, description ?: longDescription ?: shortDescription, mediumImage ?: thumbnailImage, regularPrice)
        }
    }
}