package com.fivesysdev.barscanner.utils

import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.net.Uri
import android.util.Log
import com.google.android.gms.vision.Frame
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.google.firebase.ml.vision.text.RecognizedLanguage
import java.io.File
import java.io.IOException
import java.util.*
import com.google.android.gms.vision.text.TextRecognizer
import android.provider.MediaStore
import android.graphics.Bitmap
import android.R.attr.data
import android.R.attr.text


class FirebaseUtils {

    companion object {
        const val TAG = "FirebaseUtils"
    }

    var image: FirebaseVisionImage? = null
    private lateinit var detector: FirebaseVisionBarcodeDetector
    var result = ""

    fun initialize(context: Context){
        FirebaseApp.initializeApp(context.applicationContext)
        val options = FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(
                        FirebaseVisionBarcode.FORMAT_QR_CODE,
                        FirebaseVisionBarcode.FORMAT_AZTEC,
                        FirebaseVisionBarcode.FORMAT_CODE_128,
                        FirebaseVisionBarcode.FORMAT_CODE_39,
                        FirebaseVisionBarcode.FORMAT_CODE_93,
                        FirebaseVisionBarcode.FORMAT_CODABAR,
                        FirebaseVisionBarcode.FORMAT_EAN_13,
                        FirebaseVisionBarcode.FORMAT_EAN_8,
                        FirebaseVisionBarcode.FORMAT_ITF,
                        FirebaseVisionBarcode.FORMAT_UPC_A,
                        FirebaseVisionBarcode.FORMAT_UPC_E,
                        FirebaseVisionBarcode.FORMAT_PDF417,
                        FirebaseVisionBarcode.FORMAT_DATA_MATRIX)
                .build()

        detector = FirebaseVision.getInstance().getVisionBarcodeDetector(options)
    }

    fun recognizePrice(context: Context, uri: Uri, listener: PriceListener){
        var image: FirebaseVisionImage? = null
        try {
            image = FirebaseVisionImage.fromFilePath(context, uri)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val textRecognizer = FirebaseVision.getInstance()
                .onDeviceTextRecognizer

        if (image != null)
            textRecognizer.processImage(image)
                    .addOnSuccessListener {
                        result ->
                        run {
                            Log.d(TAG, "result = $result")

                            val resultText = result.getText()
                            for (block: FirebaseVisionText.TextBlock in result.textBlocks) {
                                val blockText = block.text;
                                val blockConfidence = block.confidence;
                                val blockLanguages = block.recognizedLanguages;
                                val blockCornerPoints = block.cornerPoints;
                                val blockFrame = block.boundingBox;
                                for (line in block.lines) {
                                    val lineText = line.text;
                                    val lineConfidence = line.confidence;
                                    val lineLanguages = line.recognizedLanguages;
                                    val lineCornerPoints = line.cornerPoints;
                                    val lineFrame = line.getBoundingBox();
                                    for (element in line.elements) {
                                        val elementText = element.getText();
                                        val elementConfidence = element.getConfidence();
                                        val elementLanguages = element.getRecognizedLanguages();
                                        val elementCornerPoints = element.getCornerPoints();
                                        val elementFrame = element.getBoundingBox();
                                    }
                                }
                            }
                        }
                    }
                    .addOnFailureListener {
                        listener.onPriceRecognizeError()
                    }


        val googleTextRecognizer = TextRecognizer.Builder(context).build()
        if (googleTextRecognizer.isOperational) {
            Log.w(TAG, "Detector dependencies are not yet available.");

            val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, uri)

            val imageFrame = Frame.Builder()
                    .setBitmap(bitmap)
                    .build();

            val textBlocks = googleTextRecognizer.detect(imageFrame);

            for (i in 0..textBlocks.size()) {
                val textBlock = textBlocks.get(textBlocks.keyAt(i));
                Log.d(TAG, "textBlock = $textBlock")
                for (j in 0..textBlock.components.size){
                    val textblockcomponents = textBlocks.get(textBlocks.keyAt(j))
                    Log.d(TAG, "textblockcomponents = $textblockcomponents")
                }
                // Do something with value
            }
        }

    }

    fun recognizeBarcode(context: Context, fileName: String, listener: BarcodeListener){
        Log.d("TIME", "recognizeBarcode: start = " + Calendar.getInstance().timeInMillis)
        var image: FirebaseVisionImage? = null
        try {
            image = FirebaseVisionImage.fromFilePath(context, Uri.fromFile(File(fileName)))
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (image == null) {
            result = "Image is null"
            listener.sendResult(result)
            return
        }

        Log.d("TIME", "before detect = " + Calendar.getInstance().timeInMillis)
        val task = detector.detectInImage(image)
        task.addOnSuccessListener { barcodes ->
                    Log.d("TIME", "onsuccess = " + Calendar.getInstance().timeInMillis)
                    var expl = ""
                    var s = "Find barcodes: " + barcodes.size
                    var i = 0
                    for (barcode in barcodes) {
                        i++
                        s += "\n barcode[$i].displayValue = ${barcode.displayValue}"
                        when (barcode.valueType) {
                            FirebaseVisionBarcode.TYPE_UNKNOWN -> {
                                expl += "\n barcode[$i].valueType = TYPE_UNKNOWN"
                            }
                            FirebaseVisionBarcode.TYPE_CONTACT_INFO -> {
                                expl += "\n barcode[$i].valueType = TYPE_CONTACT_INFO"
                                val addresses = barcode.contactInfo?.addresses
                                val emails = barcode.contactInfo?.emails
                                val phones = barcode.contactInfo?.phones
                                val names = barcode.contactInfo?.name
                                val organization = barcode.contactInfo?.organization
                                val title = barcode.contactInfo?.title
                                val urls = barcode.contactInfo?.urls
                                expl += "\n addresses = $addresses, emails = $emails, phones = $phones, names = $names" +
                                        ", organization = $organization, title = $title, urls = $urls"
                            }
                            FirebaseVisionBarcode.TYPE_EMAIL -> {
                                expl += "\n barcode[$i].valueType = TYPE_EMAIL"
                                val type = barcode.email?.type
                                val address = barcode.email?.address
                                val body = barcode.email?.body
                                val subject = barcode.email?.subject
                                expl += "\n type = $type, address = $address, body = $body, subject = $subject"
                            }
                            FirebaseVisionBarcode.TYPE_ISBN -> {
                                expl += "\n barcode[$i].valueType = TYPE_ISBN"
                            }
                            FirebaseVisionBarcode.TYPE_PHONE -> {
                                expl += "\n barcode[$i].valueType = TYPE_PHONE"
                                val number = barcode.phone?.number
                                val type = barcode.phone?.type
                                expl += "\n number = $number, type = $type"
                            }
                            FirebaseVisionBarcode.TYPE_PRODUCT -> {
                                expl += "\n barcode[$i].valueType = TYPE_PRODUCT"
                            }
                            FirebaseVisionBarcode.TYPE_SMS -> {
                                expl += "\n barcode[$i].valueType = TYPE_SMS"
                                val message = barcode.sms?.message
                                val number = barcode.sms?.phoneNumber
                                expl += "\n message = $message, number = $number"
                            }
                            FirebaseVisionBarcode.TYPE_TEXT -> {
                                expl += "\n barcode[$i].valueType = TYPE_TEXT"
                            }
                            FirebaseVisionBarcode.TYPE_URL -> {
                                expl += "\n barcode[$i].valueType = TYPE_URL"
                                val title = barcode.url?.title
                                val url = barcode.url?.url
                                expl += "\n title = $title, url = $url"
                            }
                            FirebaseVisionBarcode.TYPE_WIFI -> {
                                expl += "\n barcode[$i].valueType = TYPE_WIFI"
                                val ssid = barcode.wifi?.ssid
                                val password = barcode.wifi?.password
                                val type = barcode.wifi?.encryptionType
                                expl += "\n ssid = $ssid, password = $password, type = $type"
                            }
                            FirebaseVisionBarcode.TYPE_GEO -> {
                                expl += "\n barcode[$i].valueType = TYPE_GEO"
                                val lat = barcode.geoPoint?.lat
                                val lng = barcode.geoPoint?.lng
                                expl += "\n lat = $lat, lng = $lng"
                            }
                            FirebaseVisionBarcode.TYPE_CALENDAR_EVENT -> {
                                expl += "\n barcode[$i].valueType = TYPE_CALENDAR_EVENT"
                                val description = barcode.calendarEvent?.description
                                val start = barcode.calendarEvent?.start
                                val end = barcode.calendarEvent?.end
                                val organizer = barcode.calendarEvent?.organizer
                                val summary = barcode.calendarEvent?.summary
                                val status = barcode.calendarEvent?.status
                                val location = barcode.calendarEvent?.location
                                expl += "\n description = $description, start = $start, end = $end, organizer = $organizer, summary = $summary, status = $status, location = $location"

                            }
                            FirebaseVisionBarcode.TYPE_DRIVER_LICENSE -> {
                                expl += "\n barcode[$i].valueType = TYPE_DRIVER_LICENSE"
                                val city = barcode.driverLicense?.addressCity
                                val state = barcode.driverLicense?.addressState
                                val street = barcode.driverLicense?.addressStreet
                                val zip = barcode.driverLicense?.addressZip
                                val birthDate = barcode.driverLicense?.birthDate
                                val document = barcode.driverLicense?.documentType
                                val expiry = barcode.driverLicense?.expiryDate
                                val firstName = barcode.driverLicense?.firstName
                                val middleName = barcode.driverLicense?.middleName
                                val lastName = barcode.driverLicense?.lastName
                                val gender = barcode.driverLicense?.gender
                                val issueDate = barcode.driverLicense?.issueDate
                                val issueCountry = barcode.driverLicense?.issuingCountry
                                val licenseNumber = barcode.driverLicense?.licenseNumber
                                expl += "\n city = $city, state = $state, street = $street, zip = $zip, birthDate = $birthDate, document = $document, expiry = $expiry" +
                                        ", firstName = $firstName, middleName = $middleName, lastName = $lastName, gender = $gender, issueDate = $issueDate" +
                                        ", issueCountry = $issueCountry, licenseNumber = $licenseNumber"

                            }
                        }
                    }
                    result = s
                    val rawValue : String = if (barcodes.size != 0) {
                        barcodes[0].displayValue ?: ""
                    } else ""
                    listener.sendResult(result, expl, rawValue)
                }
                .addOnFailureListener { e ->
//                    listener.sendResult("Error during detecting image", "")
                    e.printStackTrace()
                }

        Log.d("TIME", "recognizeBarcode: start = " + Calendar.getInstance().timeInMillis)

    }

}
