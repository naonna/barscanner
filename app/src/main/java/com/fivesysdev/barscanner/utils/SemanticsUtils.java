package com.fivesysdev.barscanner.utils;

import android.util.Log;

import com.semantics3.api.Products;

import org.jsonsem3.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SemanticsUtils {

    private static final String TAG = "SemanticsUtils";

    private Products products;

    public SemanticsUtils(){
        products = new Products(
                "SEM3B4B7DBC7F1E6A29EAA662F4402F900E9",
                "MTkxYTFjODZmYzU4NThjNDc0Y2UyMDQ3YTAyYmI0ZDg"
        );
    }

    public void getProductInfo(String upc){
        Log.d(TAG, "getProductInfo: upc = " + upc);
        products.productsField( "upc", upc );

        try {
            Observable<JSONObject> observable = Observable.fromCallable(() -> products.getProducts())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(json -> {
                        Log.d(TAG, "getProductInfo: " + json.toString());
                    });
            observable.subscribe();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}