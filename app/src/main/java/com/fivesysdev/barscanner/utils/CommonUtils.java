package com.fivesysdev.barscanner.utils;

import android.os.Environment;
import android.util.Log;

import com.fivesysdev.barscanner.App;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {

    private static final String TAG = "CommonUtils";

    public static File getOutputFile() {
        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
        String fileName = "IMG_" + timeStamp;
        File dir = new File(App.sApp.getCacheDir(), "images");
        if (!dir.exists()) dir.mkdirs();
        return new File(dir, fileName);
    }

    public static File getOutputMediaFile() {
        Log.d(TAG, "---->getOutputMediaFile: ");

        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
        File mediaFile;

        String id = "IMG_" + timeStamp;
        final String FOLDER_PATH = File.separator +
                Environment.DIRECTORY_PICTURES + File.separator +"BarCode" +  File.separator;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            String path = Environment.getExternalStorageDirectory() + FOLDER_PATH;
            File dir = new File(path);
            if (!dir.exists()) {
                Log.d(TAG, "" + dir + " does not exist. Creating dir");
                boolean res = dir.mkdirs();
                Log.d(TAG, "result of creating direcory: " + res);
            }
            mediaFile = new File(Environment.getExternalStorageDirectory() + FOLDER_PATH + id + ".jpeg");
        } else {
            String path = Environment.getDataDirectory() + FOLDER_PATH;
            File dir = new File(path);
            if (!dir.exists()) {
                Log.d(TAG, "" + dir + " does not exist. Creating dir");
                boolean res = dir.mkdirs();
                Log.d(TAG, "result of creating direcory: " + res);
            }
            mediaFile = new File(Environment.getDataDirectory() + FOLDER_PATH + id + ".jpeg");
        }
        try {
            boolean res = mediaFile.createNewFile();
            Log.d(TAG, "mediaFile.createNewFile(): " + res);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "mediaFile: " + mediaFile.getPath());
        Log.d(TAG, "mediaFile.getAbsolutePath: " + mediaFile.getAbsolutePath());
        Log.d(TAG, "getOutputMediaFile<----");
        return mediaFile;
    }
}
