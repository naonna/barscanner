package com.fivesysdev.barscanner.utils

interface BarcodeListener {
    fun sendResult(result: String, expl: String = "", rawValue: String = "")
    fun dialogClosed()
    fun findInfo(rawValue: String);
}
