package com.fivesysdev.barscanner.utils;

import android.util.Log;
import android.util.Size;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Camera2Utils {

    private static final String TAG = "Camera2Utils";

    private static int compare(int lhs, int rhs) {
        return Integer.compare(lhs, rhs);
    }

    private static boolean isWide(Size size) {
        double ratio = ((double) size.getWidth()) / ((double) size.getHeight());
        return ratio > 1.68 && ratio < 1.87;
    }

    public static Size findBestSize(Size[] sizeArray, long maxPicturePixels) {
        List<Size> tooLargeSizes = new ArrayList<>();
        List<Size> immutableSizeList = Arrays.asList(sizeArray);
        List<Size> sizeList = new ArrayList<>(immutableSizeList);
        Collections.sort(sizeList, (lhs, rhs) -> -compare(lhs.getWidth() * lhs.getHeight(), rhs.getWidth() * rhs.getHeight()));
        for (Size size : sizeList) {
            if (!isWide(size)) continue;
            boolean notTooLarge = ((long) size.getWidth()) * ((long) size.getHeight()) <= maxPicturePixels;
            if (!notTooLarge) {
                tooLargeSizes.add(size);
                continue;
            }
            return size;
        }
        if (tooLargeSizes.size() > 0) {
            return tooLargeSizes.get(0);
        } else {
            return sizeList.get(0);
        }
    }

    public static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        List<Size> bigEnough = new ArrayList<>();
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    static class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

}
