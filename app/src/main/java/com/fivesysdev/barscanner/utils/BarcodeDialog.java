package com.fivesysdev.barscanner.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.fivesysdev.barscanner.R;

public class BarcodeDialog {

    public static void showDialog(Activity activity, String msg, String msg2){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        TextView textMsg = (TextView) dialog.findViewById(R.id.text_msg);
        textMsg.setText(msg2);


        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog_close);
        dialogButton.setOnClickListener(v -> {
            dialog.dismiss();
            ((BarcodeListener) activity).dialogClosed();
        });

        dialog.show();

    }
}
