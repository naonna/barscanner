package com.fivesysdev.barscanner.utils

interface PriceListener {
    fun onPriceRecognize(price: String)
    fun onPriceRecognizeError()
}
