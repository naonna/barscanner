package com.fivesysdev.barscanner;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageButton;

import com.crashlytics.android.Crashlytics;
import com.fivesysdev.barscanner.utils.AutoFitTextureView;
import com.fivesysdev.barscanner.utils.BarcodeDialog;
import com.fivesysdev.barscanner.utils.BarcodeListener;
import com.fivesysdev.barscanner.utils.Camera2Utils;
import com.fivesysdev.barscanner.utils.CommonUtils;
import com.fivesysdev.barscanner.utils.FirebaseUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CameraActivityNew extends AppCompatActivity {
//        implements BarcodeListener {

    public static final String TAG = "CameraActivity";

    //CameraManager - provide all camera’s available in the device each having cameraId like front camera and back camera
    // and its properties can be obtained from CameraCharacterstics
    //Can get cameraDevice from cameraManager by it's id and perform related operations
    CameraDevice cameraDevice;

    private Size previewSize;
    private Size jpegSizes[] = null;

    public static final int PERMISSION_REQUEST_CODE_WRITE_EXTERNAL = 555;
    public static final int PERMISSION_REQUEST_CODE_OPEN_CAMERA = 5556;

    //CaptureRequest from cameradevice uses to capture images.
    private CaptureRequest.Builder previewBuilder;

    //gives CaptureRequest’s from CameraDevice.
    private CameraCaptureSession previewSession;

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    private File file;

    private ImageButton btnGetPicture;
    private AutoFitTextureView textureView;
    private View bar;
    private FirebaseUtils firebaseUtils;


    byte[] image;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    int rotatedPreviewWidth;
    int rotatedPreviewHeight;
    int maxPreviewWidth;
    int maxPreviewHeight;

    private TextureView.SurfaceTextureListener surfaceTextureListener=new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            Log.d(TAG, "onSurfaceTextureAvailable: ");
            openCamera(width, height);
        }
        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            configureTransform(width, height);
            Log.d(TAG, "onSurfaceTextureSizeChanged: ");
        }
        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            Log.d(TAG, "onSurfaceTextureDestroyed: ");
            return false;
        }
        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };
    private CameraDevice.StateCallback stateCallback=new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            Log.d(TAG, "CameraDevice.StateCallbackonOpened: ");
            cameraDevice=camera;
            startCamera();
        }
        @Override
        public void onDisconnected(CameraDevice camera) {
            Log.d(TAG, "onDisconnected: ");
            finish();
        }
        @Override
        public void onError(CameraDevice camera, int error) {
            Log.d(TAG, "onError: ");
        }
    };
    ImageReader.OnImageAvailableListener imageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Log.d(TAG, "onImageAvailable: ");
            Image image = null;
            try {
                image = reader.acquireLatestImage();
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[buffer.capacity()];
                buffer.get(bytes);
                save(bytes);
            } catch (Exception e) {
                Log.d(TAG, "Exception: " + e);
                Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
            } finally {
                if (image != null)
                    image.close();
            }
        }

        void save(byte[] bytes) {
             saveFile(bytes);
        }
    };



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        btnGetPicture = findViewById(R.id.ib_getPicture);
        textureView = findViewById(R.id.textureView);
        textureView.setSurfaceTextureListener(surfaceTextureListener);

        bar = findViewById(R.id.view_bar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        firebaseUtils = new FirebaseUtils();
        firebaseUtils.initialize(this);

        //TODO CHANGE HERE
//        file = CommonUtils.getOutputMediaFile();
        file = CommonUtils.getOutputFile();

        btnGetPicture.setOnClickListener(v -> {
            Log.d("TIME", "button clicked = " + Calendar.getInstance().getTimeInMillis());
            getPicture();
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void openCamera(int width, int height) {
        Log.d(TAG, "---->openCamera: ");
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = manager.getCameraIdList()[0];

            configureTransform(textureView.getWidth(), textureView.getHeight());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_OPEN_CAMERA);
                return;
            }

            setUpCameraOutputs(width, height);
            manager.openCamera(cameraId, stateCallback, null);
        }catch (Exception e) {
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
            Log.d(TAG, "Exception: " + e);
        }
        Log.d(TAG, "openCamera<----");
    }

    void getPicture() {
        Log.d(TAG, "--->getPicture: ");
        Log.d("TIME", "getPicture start = " + Calendar.getInstance().getTimeInMillis());
        if (cameraDevice == null) {
            Log.d(TAG, "cameraDevice == null");
            Log.d(TAG, "getPicture<----");
            return;
        }
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            Log.d(TAG, "cameraDevice: " + cameraDevice);
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Log.d(TAG, "cameraDevice.getId(): " + cameraDevice.getId());

            try {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            } catch (NullPointerException ex){
                Crashlytics.log(Log.ERROR, TAG, ex.getLocalizedMessage());
                ex.printStackTrace();
            }

            int width = 640, height = 480;

            if (jpegSizes != null && jpegSizes.length > 0) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }


            Log.d(TAG, "width: " + width + " height: " + height);
            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));
            final CaptureRequest.Builder capturebuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            capturebuilder.addTarget(reader.getSurface());
            capturebuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            Log.d(TAG, "rotation: " + rotation);
            capturebuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
            Log.d(TAG, "capturebuilder: "+ capturebuilder.get(CaptureRequest.JPEG_ORIENTATION));

            HandlerThread handlerThread = new HandlerThread("takepicture");
            handlerThread.start();
            final Handler handler = new Handler(handlerThread.getLooper());
            reader.setOnImageAvailableListener(imageAvailableListener, handler);
            final CameraCaptureSession.CaptureCallback previewSSession = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureStarted(CameraCaptureSession session, CaptureRequest request, long timestamp, long frameNumber) {
                    super.onCaptureStarted(session, request, timestamp, frameNumber);
                }

                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    Log.d(TAG, "onCaptureCompleted: ");
                }
            };
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try {
                        session.capture(capturebuilder.build(), previewSSession, handler);
                    } catch (Exception e) {
                        Log.d(TAG, "getPicture: " +  e);
                        Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
                    }
                }
                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            }, handler);
        } catch (Exception e) {
            Log.d(TAG, "getPicture: " +  e);
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
        }
        Log.d("TIME", "getPicture emd = " + Calendar.getInstance().getTimeInMillis());
        Log.d(TAG, "getPicture<----");
    }


    @Override
    protected void onPause() {
        Log.d(TAG, "---->onPause");
        super.onPause();
        if(cameraDevice!=null) {
            cameraDevice.close();
        }
        Log.d(TAG, "onPause<----");
    }

    void  startCamera() {
        Log.d(TAG, "---->startCamera:");
        if(cameraDevice==null||!textureView.isAvailable()|| previewSize ==null) {
            return;
        }
        SurfaceTexture texture=textureView.getSurfaceTexture();
        if(texture==null) {
            return;
        }
        texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
        Surface surface=new Surface(texture);
        try {
            previewBuilder=cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        }catch (Exception e) {
            Log.d(TAG, "Exception: " + e);
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
        }
        previewBuilder.addTarget(surface);
        try {
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    previewSession=session;
                    getChangedPreview();
                }
                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            },null);
        }catch (Exception e) {
            Log.d(TAG, "Exception: " + e);
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
        }
        Log.d(TAG, "startCamera<----");
    }

    void getChangedPreview() {
        Log.d(TAG, "--->getChangedPreview: ");
        if(cameraDevice==null){
            return;
        }
        previewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        HandlerThread thread=new HandlerThread("changed Preview");
        thread.start();
        Handler handler=new Handler(thread.getLooper());
        try
        {
            previewSession.setRepeatingRequest(previewBuilder.build(), null, handler);
        }catch (Exception e){
            Log.d(TAG, "Exception: " + e);
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
        }
        Log.d(TAG, "getChangedPreview<----");
    }

    private void saveFile(byte[] bytes){
        File file = CommonUtils.getOutputMediaFile();
        Log.d(TAG, "getOutputMediaFile: " + file);
        Log.d("TIME", "saveFile: start = " + Calendar.getInstance().getTimeInMillis());
//        if (file.exists())
//            file.delete();
        FileOutputStream output = null;
        try {
            Log.d("TIME", "try start = " + Calendar.getInstance().getTimeInMillis());
            if (file.exists())
                file.delete();
            output = new FileOutputStream(file);
            output.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
        } finally {
            Log.d("TIME", "finaly start = " + Calendar.getInstance().getTimeInMillis());
            try {
                if (output != null)
                    output.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception in finally " + e);
                Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
            }
            Log.d("TIME", "finally end = " + Calendar.getInstance().getTimeInMillis());
        }

        Intent returnIntent = new Intent();
        if(file!= null){
//            runOnUiThread(() -> {
//                btnGetPicture.setVisibility(View.GONE);
//                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
//            });
//            firebaseUtils.recognizeBarcode(CameraActivityNew.this, file.toString(), CameraActivityNew.this);

            returnIntent.putExtra("file",file.toString());
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }



//        Completable.fromAction(()-> {
//            Log.d("TIME", "saveFile: fromaction start = " + Calendar.getInstance().getTimeInMillis());
//            FileOutputStream output = null;
////            Bitmap bitmap = getBitmap(bytes);
//            try {
//                Log.d("TIME", "try start = " + Calendar.getInstance().getTimeInMillis());
//                if (file.exists())
//                    file.delete();
//                output = new FileOutputStream(file);
//                output.write(bytes);
//                Log.d("TIME", "compress start = " + Calendar.getInstance().getTimeInMillis());
////                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, output);
//                Log.d("TIME", "compress end = " + Calendar.getInstance().getTimeInMillis());
//            } catch (Exception e) {
//                e.printStackTrace();
//                Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
//            } finally {
//                Log.d("TIME", "finaly start = " + Calendar.getInstance().getTimeInMillis());
//                try {
//                    if (output != null)
//                        output.close();
//                } catch (Exception e) {
//                    Log.d(TAG, "Exception in finally " + e);
//                    Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
//                }
//                Log.d("TIME", "finally end = " + Calendar.getInstance().getTimeInMillis());
//            }
//            Log.d("TIME", "saveFile: fromaction end = " + Calendar.getInstance().getTimeInMillis());
//        }).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()).doOnComplete(()->{
//            Log.d("TIME", "saveFile: oncomplete start = " + Calendar.getInstance().getTimeInMillis());
//            firebaseUtils.recognizeBarcode(CameraActivityNew.this, file.toString(), CameraActivityNew.this);
//            Log.d("TIME", "saveFile: oncomplete end = " + Calendar.getInstance().getTimeInMillis());
//        }).subscribe();
    }

    private Bitmap getBitmap(byte[] bytes) {
        Log.d("TIME", "getBitmap start = " + Calendar.getInstance().getTimeInMillis());
        Bitmap imageOriginal = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        float scale = 1;

        int orientation = getResources().getConfiguration().orientation;
        if ((orientation == Configuration.ORIENTATION_PORTRAIT && imageOriginal.getWidth() > imageOriginal.getHeight())
                || (orientation == Configuration.ORIENTATION_LANDSCAPE && imageOriginal.getWidth() < imageOriginal.getHeight())) {
            imageOriginal = rotateBitmap(imageOriginal, 90);
        }

        final int originalHeight = imageOriginal.getHeight();
        final int originalWidth = imageOriginal.getWidth();
        final int barTop = bar.getTop();
        final int barBottom = bar.getBottom();
        final int barLeft = bar.getLeft();
        final int barRight = bar.getRight();
        final int barWidth = bar.getWidth();
        final int barHeight= bar.getHeight();

        final int left;
        final int width;
        final int height;
        final int  top;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE){
//                width = (int) (scale*((bar.getLeft() + bar.getRight()) / 1.5));
//                height = (int) (scale*((bar.getTop() + bar.getBottom()) / 1.5));
//                top = (int) ((imageOriginal.getHeight() - height) / 2.2);
//                left =  (int) ((imageOriginal.getWidth() - width) / 2.2);
//
            top = (int) (scale* barTop);
//                height = originalHeight - top*2;
            left = (barLeft + barRight) / 2;
            width = originalWidth - left*2;
            height = (int) (width / 1.6);
        } else {
            left =  (int) (scale*barLeft);
            top = (barTop + barBottom)/ 2;
//                height =  (originalHeight - (top *2)) ;
            width = (int) originalWidth - left*2;
//                height = originalHeight  - (2 * top);
            height = (int) (width / 1.6);
        }
        //TODO check this koef

        Log.d(TAG, "left: " + left + ", top = " + top + ", width = " + width + ", height = " + height);

        Bitmap smallBitmap;
        try {
            int realTop;
            if (top < 0) realTop = (barTop + barBottom)/ 2;
            else realTop = top;
            Log.d(TAG, "left: " + left + ", realTop = " + realTop + ", width = " + width + ", height = " + height);
            Log.d(TAG, "barWidth: " + barWidth + ", barHeight = " + barHeight + ", barTop =  " + barTop + ", barbottom = " + barBottom);
            Log.d(TAG, "originalHeight: " + originalHeight + ", originalWidth = " + originalWidth);
            smallBitmap = Bitmap.createBitmap(imageOriginal, left, realTop, width, height);
        } catch (IllegalArgumentException ex){
            ex.printStackTrace();
            Crashlytics.log(Log.ERROR, TAG, ex.getLocalizedMessage());
            smallBitmap = imageOriginal;
        }

        Log.d("TIME", "getBitmap end = " + Calendar.getInstance().getTimeInMillis());
        return smallBitmap;

    }

    public Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        Log.d(TAG, "---->configureTransform: ");
        Log.d(TAG, "viewWidth: " + viewWidth + " viewHeight: " + viewHeight);

        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String camerId = manager.getCameraIdList()[0];
            Log.d(TAG, "camerId: " + camerId);
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(camerId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            //this part for debug
//            Size[] prev = map.getOutputSizes(SurfaceTexture.class);
//            for (int i = 0; i < prev.length; i++) {
//                Log.d(TAG, " " +  prev[i]);
//            }

            Point displaySize = new Point();
            getWindowManager().getDefaultDisplay().getSize(displaySize);

            rotatedPreviewWidth = viewWidth;
            rotatedPreviewHeight = viewHeight;
            maxPreviewWidth = displaySize.x;
            maxPreviewHeight = displaySize.y;

            Size largest = map.getOutputSizes(SurfaceTexture.class)[0];

            previewSize = Camera2Utils.chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                    maxPreviewHeight, largest);

            Log.d(TAG, "previewSize SurfaceTexture: " + previewSize);
        } catch (CameraAccessException ex){
            ex.printStackTrace();
            Crashlytics.log(Log.ERROR, TAG, ex.getLocalizedMessage());
        }

        Log.d(TAG, "previewSize: getWidth: " + previewSize.getWidth() + "getHeight: " + previewSize.getHeight());

        if (null == textureView || null == previewSize) {
            Log.d(TAG, "textureView or previewSize is null. return");
            return;
        }

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, previewSize.getHeight(), previewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / previewSize.getHeight(),
                    (float) viewWidth / previewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }

        Log.d(TAG, "previewSize configureTransform: " + previewSize);
        textureView.setTransform(matrix);

        Log.d(TAG, "configureTransform<----");
    }

    private void setUpCameraOutputs(int width, int height) {
        Log.d(TAG, "---->setUpCameraOutputs: ");
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);

                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }

                StreamConfigurationMap map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                // For still image captures, we use the largest available size.
                Size largest = Collections.max(
                        Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                        new CompareSizesByArea());


                Point displaySize = new Point();
                getWindowManager().getDefaultDisplay().getSize(displaySize);

                rotatedPreviewWidth = width;
                rotatedPreviewHeight = height;
                maxPreviewWidth = displaySize.x;
                maxPreviewHeight = displaySize.y;

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                previewSize = Camera2Utils.chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight, largest);

                // We fit the aspect ratio of TextureView to the size of preview we picked.
                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    textureView.setAspectRatio(
                            previewSize.getWidth(), previewSize.getHeight());
                } else {
                    textureView.setAspectRatio(
                            previewSize.getHeight(), previewSize.getWidth());
                }

            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
            Crashlytics.log(Log.ERROR, TAG, e.getLocalizedMessage());
        }
        Log.d(TAG, "setUpCameraOutputs<----");
    }

//    @Override
//    public void sendResult(String result, String expl, String rawValue) {
//        BarcodeDialog.showDialog(this, result, expl);
//        runOnUiThread(() -> {
//            findViewById(R.id.progressBar).setVisibility(View.GONE);
//            btnGetPicture.setVisibility(View.VISIBLE);
//        });
//
//    }
//
//    @Override
//    public void dialogClosed() {
//        startCamera();
//    }

    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

}
